import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class kadai {

    private JPanel root;
    private JButton checkOutButton;
    private JTextPane textPane1;
    private JButton wakameButton;
    private JButton sanratanButton;
    private JButton kitsuneButton;
    private JButton siobutanegiButton;
    private JButton kakeButton;
    private JButton ontamaButton;
    private JButton kareButton;
    private JButton yuzutororoButton;
    private JButton kinokoButton;
    private JButton eatInButton;
    private JButton takeOutButton;
    private JButton startButton;
    private JTextPane textPane11;

    int total = 0;

    void order(String food, int kakaku){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0){
            String currentText = textPane1.getText();
            textPane1.setText(food+" "+kakaku);
            String NEW_TEXT = textPane1.getText();
            JOptionPane.showMessageDialog(null,"Order for "+food+" received.");
            textPane1.setText(currentText + NEW_TEXT +"\n");

            total += kakaku;
            textPane11.setText(total +"\n");

        }




    }
    public kadai() {
        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                kakeButton.setIcon(new ImageIcon(
                        this.getClass().getResource("kake.png")
                ));
                wakameButton.setIcon(new ImageIcon(
                        this.getClass().getResource("wakame.png")
                ));
                sanratanButton.setIcon(new ImageIcon(
                        this.getClass().getResource("sanratan.png")
                ));
                kitsuneButton.setIcon(new ImageIcon(
                        this.getClass().getResource("kitsune.png")
                ));
                siobutanegiButton.setIcon(new ImageIcon(
                        this.getClass().getResource("siobutanegi.png")
                ));
                ontamaButton.setIcon(new ImageIcon(
                        this.getClass().getResource("ontama.png")
                ));
                kareButton.setIcon(new ImageIcon(
                        this.getClass().getResource("kare.png")
                ));
                yuzutororoButton.setIcon(new ImageIcon(
                        this.getClass().getResource("yuzutororo.png")
                ));
                kinokoButton.setIcon(new ImageIcon(
                        this.getClass().getResource("kinoko.png")
                ));

            }
        });
        kakeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("kake",100);
            }
        });
        wakameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("wakame",110);
            }
        });
        kitsuneButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("kitsune",120);
            }
        });
        ontamaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("egg",130);
            }
        });
        sanratanButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("hot and sour",200);
            }
        });
        kareButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("curry",210);
            }
        });
        siobutanegiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("solt and pork",220);
            }
        });
        yuzutororoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("yuzu and grated yam",230);
            }
        });
        kinokoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("mushroom",240);
            }
        });
        takeOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to take out?",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you. We will discount.");
                    total = total / 2;
                    textPane11.setText(total + "\n");
                }


            }
        });
        eatInButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to eat in?",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Ok.");
                }
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String selectvalues[] = {"shrimp", "yam", "nothing"};
                int select = JOptionPane.showOptionDialog(root,
                        "Tempra",
                        "Do you want tempra?",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        selectvalues,
                        selectvalues[0]);

                if (select==JOptionPane.CLOSED_OPTION){
                    textPane1.setText("no answer");}
                else if(select==0){
                    order("tempra[shrimp]",50);
                }
                else if(select==1){
                    order("tempra[yam]",40);
                }
                else{
                    order("tempra[nothing]",0);
                }


                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is "+total+" yen");
                    total=0;
                    textPane11.setText(total + "\n");
                    textPane1.setText("\n");

                }


            }
        });
    }


    public static void main(String[] args) {
        JFrame frame = new JFrame("kadai");
        frame.setContentPane(new kadai().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
